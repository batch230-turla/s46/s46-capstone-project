const mongoose =require("mongoose");
const Product = require("../models/product.js");


module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		Available: reqBody.Available
	})

	return newProduct.save().then((newProduct,error)=>{
		if(error){
			return error;

		}
		else{

			return newProduct;
		}
	})
}
//Get all product
module.exports.getAllProduct =()=>{
	return Product.find({}).then(result=>{
		return result;
	})
}

//Get all active product

module.exports.getActiveProduct =()=>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
}


//Get specific product
module.exports.getProduct =(productId)=>{
	return Product.findById(productId).then(result=> {
		return result;
	})
}

//updating a product

module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				 
				description: newData.product.description,
				
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//_______________________for archive product__________________

module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,

			{
				
				isActive: false
				
			}
		).then((result, error)=>{
			if(error){
				return true;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('You have already Archive Product');
		return message.then((value) => {return value});
	}
}
const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");



module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
		//
		})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}

		})
	}
//for postman s37 checkemail
		module.exports.checkEmailExist = (reqBody) =>{
			return User.find({email: reqBody.email}).then(result => {
				if(result.length > 0){
					return true;
				}
				else{
					return false;
				}
			})
		}
//for postman s37 login
		module.exports.loginUser = (reqBody)=>{
			return User.findOne({email: reqBody.email}).then(result =>{
				if (result == null){
					return false;
				}
				else{
					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
					const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password );
					//true of false

					if(isPasswordCorrect){
						//lets give the user a token to a access features
						return {access: auth.createAccessToken(result)};
					}
					else{
						//if the password does not match, else
						return false;
					}
				}
			})

		}


		module.exports.getAllUserController = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}


//loginDetails

		module.exports.getAllUserController = (reqBody)=>{
			return User.findById(reqBody._id).then(result =>{
				if (result == null){
					return false;
				}
				else{
					//compareSync is a bcrypt function to compare unhashed passsword to hash pasword
					const isIdCorrect = bcrypt.compareSync(reqBody.Id, result.Id );
					//true of false

					if(isIdCorrect){
						//lets give the user a token to a access features
						return {access: auth.createAccessToken(result)};
					}
					else{
						//if the password does not match, else
						return true;
					}
				}
			})
		}




module.exports.getProfile = (request, response) => {
	
	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);
	
	return User.findById(userData.id).then(result => {
		result.password = "*****";
		response.send(result);
	})
}



module.exports.Checkout = async (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	let productName = await product.findById(request.body.productId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header (request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		productId: request.body.productId,
		productName: productName
	}

	console.log(newData);

	// a user document is updated if we received a "true" value
	let isUserUpdated = await User.findById(newData.userId)
	.then(user => {
		user.enrollments.push({
			productId: newData.productId,
			productName: newData.productName
		});
		// Save the updated user information from the database
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isProductUpdated = await Product.findById(newData.productId).then(product => {

		product.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
	
		// Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a message in the terminal that the slot is already zero
		// [3] Else if the slot is a negative value, it should make the slot value to zero

		// Minus the slots available by 1
		// course.slots = course.slots - 1;
		product.slots -= 1;

		return product.save()
		.then(result=>{
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isProductUpdated);

	// Condition will check if the both "user" and "course" document has been updated.
	// Ternary operator
	(isUserUpdated == true &&  isProductUpdated == true)? response.send(true) : response.send(false);

	/*
		if(isUserUpdated == true &&  isCourseUpdated == true){
			response.send(true);
		}
		else{
			response.send(false);
		}
	*/
}

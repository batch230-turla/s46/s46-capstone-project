const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth =require("../auth.js");



//creating a product

//___________________________


router.post("/newProduct", (request, response)=>{
	productControllers.addProduct(request.body).then(resultFromController =>response.send(
			resultFromController))
});



//___________________________
router.post("/create", (request, response)=>{
	productControllers.addProduct(request.body).then(resultFromController =>response.send(
			resultFromController))
});

//Get all product
router.get("/all",(request, response)=>{
	productControllers.getlAllProduct().then(resultFromController =>response.send(resultFromController))
})


//Get all ACtivee product

router.get("/active",(request, response)=>{
	productControllers.getActiveProduct().then(resultFromController =>response.send(resultFromController))
});


//____________product id

router.get("/:productId", (request, response)=>{
	productControllers.getProduct(request.params.productId).then(resultFromController => response.send(resultFromController))
});

//____________update product

router.patch("/:productId/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.updateProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


//______________________archive product

router.patch("/:productId/archive", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	}	)
})


router.post("/checkout", (request, response)=>{
	productControllers.addProduct(request.body).then(resultFromController =>response.send(
			resultFromController))
});




module.exports = router;
